package br.com.itau.empresas.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    EmpresaProducer empresaProducer;

    @PostMapping
    public Empresa cadastrar(@RequestBody Empresa empresa){
         empresaProducer.enviarAoKafka(empresa);
         return empresa;
    }
}
