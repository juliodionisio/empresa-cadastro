package br.com.itau.empresas.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaProducer {

    @Autowired
    KafkaTemplate<String, Empresa> producer;
    public void enviarAoKafka(Empresa empresa) {
        producer.send("spec3-julio-felipe-2", empresa);
    }
}
